public class LinkList2App {
    public static void main(String[] args) {
        LinkList thelist = new LinkList();
        thelist.insertFirst(22,2.99);
        thelist.insertFirst(44,4.99);
        thelist.insertFirst(66,6.99);
        thelist.insertFirst(88,8.99);

        thelist.displayList();

        Link f = thelist.find(88);
        if (f != null) System.out.println("Found link with key " + f.iData);
        else System.out.println("Can't find link");
        
        Link d = thelist.delete(66);
        if (d != null) System.out.println("Deleted link with key + " + d.iData);
        else System.out.println("Can't delete link");

        thelist.displayList();
    }
}

public class LinkListApp {
    public static void main(String[] args) {
        LinkList thelist = new LinkList();

        thelist.insertFirst(22,2.99);
        thelist.insertFirst(44,4.99);
        thelist.insertFirst(66,6.99);
        thelist.insertFirst(88,8.99);
        thelist.insertLast(11,1.99);
        thelist.displayList();
        thelist.deleteLast();
        thelist.displayList();
        while(!thelist.isEmpty()) {
            Link aLink = thelist.deleteFirst();
            System.out.println("Deleted ");
            aLink.displayLink();
            System.out.println();
            System.out.println("--------------------------------");
        } thelist.displayList();
    }
}
